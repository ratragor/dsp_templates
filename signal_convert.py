import math
import struct

import matplotlib.pyplot as plt

F = (5, 8, 10, 12, 15, 18)
A = (3, 10, 2, 5, 7, 13)

Fd = max(F)*3
print(Fd)

N = 64
tn = [i*(1/Fd) for i in range(N)]
signal = [0 for i in range(N)]

for i in range(len(F)):
    for j in range(N):
        signal[j] += A[i]*math.sin(2*math.pi*F[i]*tn[j])

signal_int = [int(signal[i]) for i in range(N)]                  # convert float to integer
signal_hex = [signal[i].hex() for i in range(N)]
print(signal)
print(signal_int)
print(signal_hex)

def float_to_hex(f):                                            # get hex from float point number 32 bit
    return hex(struct.unpack('<I', struct.pack('<f', f))[0])

def double_to_hex(f):                                           # get hex from float point number 64 bit
    return hex(struct.unpack('<Q', struct.pack('<d', f))[0])

signal_hex = [float_to_hex(signal[i]) for i in range(N)]
print(signal_hex)
signal_hex = [double_to_hex(signal[i]) for i in range(N)]
print(signal_hex)

plt.plot(tn, signal, 'r', tn, signal_int, 'b')
plt.xlabel('time')
plt.ylabel('power')
plt.show()

signal_int = [hex(int(signal[i])) for i in range(N)]            # get hex from integer number 32 bit
print(signal_int)

b = [0 for i in range(16)]

b[0] = 8.58398885*10**-3
b[1] = 1.01415502*10**-2
b[2] =-1.18238033*10**-2
b[3] =-4.97749198*10**-2
b[4] =-5.10048196*10**-2
b[5] = 3.87851131*10**-2
b[6] = 2.01367094*10**-1
b[7] = 3.33522835*10**-1

b[15]= 8.58398885*10**-3
b[14]= 1.01415502*10**-2
b[13]=-1.18238033*10**-2
b[12]=-4.97749198*10**-2
b[11]=-5.10048196*10**-2
b[10]= 3.87851131*10**-2
b[9] = 2.01367094*10**-1
b[8] = 3.33522835*10**-1

print(b)

